# Initialization

1. Create react app using,
    `npx create-react-app learnredux`

2. Install redux packages
    ```
    cd learnredux
    npm i -s redux react-redux
    ```

So general way is to create two folders, **actions** & **reducers** inside **src**
folder.

Then before we wrap our global `<App />` in the `Provider` from `'react-redux'` we will
define reducers and actions for our components.

# Reducers

Inside **reducers** and **actions** folder create file name *index.js*.

In **reducers/index.js** file we will define a `combineReducers` which will combine all
the reducers.

We have two different reducers namely,

1. `counterReducer` in **reducers/counter.js** file in which we use switch case for
checking two cases, `INCREMENT` & `DECREMENT` and we simply increment or decrement the
state with value one.

2. `isLoggedReducer` in **reducers/isLogged.js** file in which we simply check if
action's type is `SIGN_IN` then we return the opposite of the `state` else in default
case we simply return the `state` itself.

Now we can simply combine the reducers using `combineReducers`. So, in **reducers/index.js** we will import both the reducers and combine them,

```
import { combineReducers } from 'react-redux';
...

const allReducers = combineReducers({
    counter: counterReducer,  // you can name 'counter' anything
    isLogged: isLoggedReducer
})

export default allReducers
```

Now before creating actions we can create a store for our provider,

# Store

```
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import allReducers from './reducers';

const store = createStore(
    allReducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
```

# Actions
Inside **actions/index.js** we will define two actions which are basically functions
which will return a object,

1. `increment`
2. `decrement`

Both functions just returns the type of action which is either `INCREMENT` or `DECREMENT`.

# Dispatching Actions
We can dispatch actions inside any component, we won't create any new component and just
use the default `App` component along with two new functions, `useSelector` & `useDispatch`.

1. `useSelector` is used to access the state in any component.
2. `useDispatch` is used to dispatch any action.

```
import { useSelector, useDispatch } from 'react-redux';

import { increment, decrement } from './actions';

function App() {
    const count = useSelector(state => state.counter);
    const isLogged = useSelector(state => state.isLogged);

    const dispatch = useDispatch();

    return (
        <div className="App">
            <h1>Counter: { count }</h1>
            <button onClick={() => dispatch(increment(5))}>+</button>
            <button onClick={() => dispatch(decrement())}>-</button>

            {isLogged ? <h3>Only visible to logged in users.</h3> : ""}
        </div>
  );
}

export default App;
```

We simply get the `counter` and `isLogged` value from our redux state and use it to
display content.

Then to dispatch actions using buttons we use `useDispatch` and pass our action
functions with any arguments if exists.
